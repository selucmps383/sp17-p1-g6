﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CMPS383Phase1.Models
{
    public class InventoryItem
    {
        [Key]
        public int InventoryItemID { get; set; }
        [Required]
        public int CreatedByUserId { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public int Quantity { get; set; }

        public User CreatedByUser { get; set; }

    }
}
