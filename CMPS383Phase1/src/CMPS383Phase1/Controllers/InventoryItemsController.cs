using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CMPS383Phase1.Data;
using CMPS383Phase1.Models;
using Microsoft.AspNetCore.Authorization;

namespace CMPS383Phase1.Controllers
{
    public class InventoryItemsController : Controller
    {
        private readonly Context _context;

        
        public InventoryItemsController(Context context)
        {
            _context = context;    
        }
        
        public async Task<IActionResult> Index()
        {
            var context = _context.InventoryItems.Include(i => i.CreatedByUser);
            return View(await context.ToListAsync());
        }
        
        [Authorize]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var inventoryItem = await _context.InventoryItems
                .Include(i => i.CreatedByUser)
                .SingleOrDefaultAsync(m => m.InventoryItemID == id);
            if (inventoryItem == null)
            {
                return NotFound();
            }

            return View(inventoryItem);
        }

        [Authorize(Roles = "ADMIN")]
        public IActionResult Create()
        {
            ViewData["CreatedByUserId"] = new SelectList(_context.Users, "UserID", "FirstName");
            return View();
        }

        [Authorize(Roles = "ADMIN")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("InventoryItemID,CreatedByUserId,Name,Quantity")] InventoryItem inventoryItem)
        {
            if (ModelState.IsValid)
            {
                _context.Add(inventoryItem);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewData["CreatedByUserId"] = new SelectList(_context.Users, "UserID", "FirstName", inventoryItem.CreatedByUserId);
            return View(inventoryItem);
        }

        [Authorize(Roles = "ADMIN")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var inventoryItem = await _context.InventoryItems.SingleOrDefaultAsync(m => m.InventoryItemID == id);
            if (inventoryItem == null)
            {
                return NotFound();
            }
            ViewData["CreatedByUserId"] = new SelectList(_context.Users, "UserID", "FirstName", inventoryItem.CreatedByUserId);
            return View(inventoryItem);
        }

        [Authorize(Roles = "ADMIN")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("InventoryItemID,CreatedByUserId,Name,Quantity")] InventoryItem inventoryItem)
        {
            if (id != inventoryItem.InventoryItemID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(inventoryItem);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!InventoryItemExists(inventoryItem.InventoryItemID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            ViewData["CreatedByUserId"] = new SelectList(_context.Users, "UserID", "FirstName", inventoryItem.CreatedByUserId);
            return View(inventoryItem);
        }
        [Authorize(Roles = "ADMIN")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var inventoryItem = await _context.InventoryItems
                .Include(i => i.CreatedByUser)
                .SingleOrDefaultAsync(m => m.InventoryItemID == id);
            if (inventoryItem == null)
            {
                return NotFound();
            }

            return View(inventoryItem);
        }

        [Authorize(Roles = "ADMIN")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var inventoryItem = await _context.InventoryItems.SingleOrDefaultAsync(m => m.InventoryItemID == id);
            _context.InventoryItems.Remove(inventoryItem);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }


        public async Task<IActionResult> Purchase(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var inventoryItem = await _context.InventoryItems.SingleOrDefaultAsync(m => m.InventoryItemID == id);
            if (inventoryItem == null)
            {
                return NotFound();
            }
            return View(inventoryItem);
        }
        [HttpPost, ActionName("Purchase")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Purchase(int id)
        {
            var inventoryItem = await _context.InventoryItems.SingleOrDefaultAsync(m => m.InventoryItemID == id);
            inventoryItem.Quantity = inventoryItem.Quantity;
            if (inventoryItem.Quantity == 0)
            {
                inventoryItem.Quantity = 0;
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            else
            {
                inventoryItem.Quantity = inventoryItem.Quantity - 1;
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
        }

        private bool InventoryItemExists(int id)
        {
            return _context.InventoryItems.Any(e => e.InventoryItemID == id);
        }
    }
}
