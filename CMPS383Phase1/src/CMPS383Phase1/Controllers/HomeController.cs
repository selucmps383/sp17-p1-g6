﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CMPS383Phase1.Data;
using CMPS383Phase1.Models;
using System.Security.Claims;
using CryptoHelper;
using Microsoft.AspNetCore.Authorization;

namespace CMPS383Phase1.Controllers
{
    public class HomeController : Controller
    {
        private readonly Context _context;
        public HomeController(Context context)
        {
            _context = context;
        }
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult Login()
        {
            return View();
        }
        [Authorize]
        [HttpPost]
        public async Task<IActionResult> Logout()
        {
            await HttpContext.Authentication.SignOutAsync("MyCookieMiddlewareInstance");
            return RedirectToAction("Login", "Home");
        }
        [HttpPost]
        public async Task<IActionResult> LoginAccount(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                if(model.Username == "admin")
                {
                    if (Crypto.VerifyHashedPassword(_context.Users.Where(s => s.Username == model.Username).Select(s => s.Password).FirstOrDefault(), model.Password))
                    {
                        var claims = new List<Claim>()
                        {
                            new Claim(ClaimTypes.NameIdentifier, model.Username)
                        };
                        var identity = new ClaimsIdentity(claims, model.Username);
                        identity.AddClaim(new Claim(ClaimTypes.Role, "ADMIN"));

                        var claimsPrincipal = new ClaimsPrincipal(identity);
                        await HttpContext.Authentication.SignInAsync("MyCookieMiddlewareInstance", claimsPrincipal);
                        return RedirectToAction("Index", "InventoryItems");
                    }
                    else
                    {
                        ModelState.AddModelError("", "Invalid login attempt");
                        return RedirectToAction("Login", "Home");
                    }
                }
                else if (_context.Users.Where(s => s.Username == model.Username).FirstOrDefault() != null)
                {
                    
                    if (Crypto.VerifyHashedPassword(_context.Users.Where(s => s.Username == model.Username).Select(s => s.Password).FirstOrDefault(), model.Password))
                    {
                        var claims = new List<Claim>()
                        {
                            new Claim(ClaimTypes.NameIdentifier, model.Username)
                        };
                        var identity = new ClaimsIdentity(claims, model.Username);
                        identity.AddClaim(new Claim(ClaimTypes.Role, "USER"));

                        var claimsPrincipal = new ClaimsPrincipal(identity);
                        await HttpContext.Authentication.SignInAsync("MyCookieMiddlewareInstance", claimsPrincipal);
                        return RedirectToAction("Index", "InventoryItems");
                    }
                    else
                    {
                        ModelState.AddModelError("", "Invalid login attempt");
                        return RedirectToAction("Login", "Home");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Invalid login attempt");
                    return RedirectToAction("Login", "Home");
                }
            }
            ModelState.AddModelError("", "Invalid login attempt");
            return RedirectToAction("Login", "Home");
        }
    }
}
