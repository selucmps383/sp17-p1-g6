﻿using CMPS383Phase1.Models;
using Microsoft.EntityFrameworkCore;

//calls to the Data Folder
namespace CMPS383Phase1.Data
{
    public class Context : DbContext
    {
        public Context(DbContextOptions<Context> options) : base(options)
        {

        }
        //verifies that the username is unique
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
                .HasIndex(b => b.Username).IsUnique();
        }


        //When the database gets generated it will create a table with the names "User" and "InventoryItem".
        public DbSet<User> Users { get; set; }
        public DbSet<InventoryItem> InventoryItems { get; set; }
    }
}