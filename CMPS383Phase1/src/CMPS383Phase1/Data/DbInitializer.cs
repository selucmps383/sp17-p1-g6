﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using CMPS383Phase1.Data;
using CryptoHelper;

namespace CMPS383Phase1.Models
{
    public static class DbInitializer
    {
        public static void Initialize(Context context)
        {
            context.Database.EnsureCreated();

            if(context.Users.Any())
            {
                return;
            }

            var users = new User[]
            {
              
                new User { Username="admin",FirstName="Admin",LastName="Account",Password=Crypto.HashPassword("selu2017") },
                new User { Username="jsmith",FirstName="John",LastName="Smith",Password=Crypto.HashPassword("S3curEpa$5w0rd") },
                new User { Username="jsmith2",FirstName="Jane",LastName="Smith",Password=Crypto.HashPassword("password") },
                new User { Username="jjones",FirstName="Jill",LastName="Jones",Password=Crypto.HashPassword("password") },
                new User { Username="jjenkins",FirstName="Jim",LastName="Jenkins",Password=Crypto.HashPassword("password") }
            };
            foreach(User s in users)
            {
                context.Users.Add(s);
            }
            context.SaveChanges();

            var inventoryitems = new InventoryItem[]
            {
                new InventoryItem { CreatedByUserId=1,Name="Cup",Quantity=400 },
                new InventoryItem { CreatedByUserId=3,Name="Plate",Quantity=250 },
                new InventoryItem { CreatedByUserId=5,Name="Fork",Quantity=320 },
                new InventoryItem { CreatedByUserId=4,Name="Spoon",Quantity=290 },
                new InventoryItem { CreatedByUserId=2,Name="Knife",Quantity=150 }
            };
            foreach(InventoryItem i in inventoryitems)
            {
                context.InventoryItems.Add(i);
            }
            context.SaveChanges();
        }
    }
}
