## Technical Requirements ##

* Database must be generated and seeded using Entity Framework, Code First migrations.  

* When pulling your project down to present, we will run the update database command to create your database - it cannot exist beforehand. 

* Be sure to seed an “admin” user with the password “selu2017” 

* You are not to use the “default” ASP.NET Identity.  

* Usernames must be unique (and enforced at the database level) 

* Passwords must be hashed 

* Verify the password that the user entered against the hash stored in the database (lookup by Username). 
 
* Once the user enters the correct username/password, you can authenticate them using cookie middleware. 

* Only the public login section, the inventory level viewing page, and the purchasing endpoints may be accessible when not logged in (Use the Authorize attribute to secure your site). 

* No Ids, passwords, or other “unfriendly” types of data that are irrelevant to the users of the site should appear on any web pages. 
 

## User Stories ##

* As an administrator, I should be able to login using my username/password in order to access the back-end management portal. 

* As an administrator, I should be able to list, add, edit, and delete users from the system. 

* As an administrator, I should be able to list, add, edit, and delete inventory items from the system. 

* As a user, I should be able to go to a public page without logging in, in order to see the current inventory levels. 

* As a user, I should be able to submit a GET request to an inventory endpoint, in order to retrieve inventory levels from the system. 

* As a user, I should be able to execute a POST request to an inventory endpoint, in order to purchase an item (and decrease the inventory level).